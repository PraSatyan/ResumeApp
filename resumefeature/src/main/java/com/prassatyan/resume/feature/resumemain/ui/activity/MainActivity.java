package com.prassatyan.resume.feature.resumemain.ui.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.oss.licenses.OssLicensesMenuActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.prassatyan.resume.feature.resumemain.R;
import com.prassatyan.resume.feature.resumemain.ui.fragment.github.fragment.GithubFragment;
import com.prassatyan.resume.feature.resumemain.ui.fragment.linkedin.LinkedinFragment;
import com.prassatyan.resume.feature.resumemain.ui.fragment.resume.ResumeFragment;
import com.prassatyan.resume.feature.resumemain.ui.fragment.settings.SettingsFragment;
import com.prassatyan.resume.ui.ResumeBaseActivity;
import com.praszapps.janus.manager.JanusAuthenticationCallback;
import com.praszapps.janus.manager.JanusAuthenticationResponse;
import com.praszapps.janus.manager.JanusAuthenticationStyle;
import com.praszapps.janus.manager.JanusAuthenticator;

import org.jetbrains.annotations.NotNull;

import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;


public class MainActivity extends ResumeBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean showBiometricLogin = preferences.getBoolean("login_using_biometric", false);
        if (showBiometricLogin) {
            JanusAuthenticator.INSTANCE.authenticate(JanusAuthenticationStyle.BIOMETRIC_DIALOG, this, new JanusAuthenticationCallback() {
                @Override
                public void onAuthenticationResponse(@NotNull JanusAuthenticationResponse janusAuthenticationResponse) {
                    if (janusAuthenticationResponse instanceof JanusAuthenticationResponse.Success) {
                        initMainUI();
                    } else {
                        finish();
                    }
                }
            });
        } else {
            //TODO show screen to display access code and then login
            initMainUI();
        }
    }


    private void initMainUI() {
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_placeholder, new ResumeFragment()).commit();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        // TODO implement data binding once fragment switching works
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(menuItem -> {
            if (menuItem.getItemId() == com.prassatyan.resume.R.id.menu_item_resume) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_placeholder, new ResumeFragment()).commitAllowingStateLoss();
            } else if (menuItem.getItemId() == com.prassatyan.resume.R.id.menu_item_github) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_placeholder, new GithubFragment()).commitAllowingStateLoss();
            } else if (menuItem.getItemId() == com.prassatyan.resume.R.id.menu_item_linkedin) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_placeholder, new LinkedinFragment()).commitAllowingStateLoss();
            } else if (menuItem.getItemId() == com.prassatyan.resume.R.id.menu_item_settings) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_placeholder, new SettingsFragment()).commitAllowingStateLoss();
            }
            return true;
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_item_license) {
            startActivity(new Intent(this, OssLicensesMenuActivity.class));
        }
        return true;
    }
}